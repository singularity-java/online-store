package kz.jusan.mailing.service;

import kz.jusan.mailing.dto.OrderDto;
import kz.jusan.mailing.dto.PaymentConfirmDto;

public interface MailService {
    void sendMail(OrderDto orderDto);

    void sendMailPayment(PaymentConfirmDto paymentConfirmDto);
}

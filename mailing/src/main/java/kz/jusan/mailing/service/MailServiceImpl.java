package kz.jusan.mailing.service;

import kz.jusan.mailing.dto.OrderDto;
import kz.jusan.mailing.dto.PaymentConfirmDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {
    private final JavaMailSender javaMailSender;
    @Override
    public void sendMail(OrderDto orderDto) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("mtorezhan@gmail.com");
        message.setTo(orderDto.getEmail());
        message.setSubject("Order");
        message.setText("You need to pay $" + orderDto.getAmount());
        javaMailSender.send(message);

        log.info("Message sent to " + orderDto.getEmail());
    }

    @Override
    public void sendMailPayment(PaymentConfirmDto paymentConfirmDto) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("narutoship334.380@gmail.com");
        message.setTo(paymentConfirmDto.getEmail());
        message.setSubject(paymentConfirmDto.getSubject());
        message.setText(paymentConfirmDto.getMessage());
        javaMailSender.send(message);

        log.info("Message sent to " + paymentConfirmDto.getEmail());
    }
}

package kz.jusan.mailing.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {
    public static final String MAIL_QUEUE = "mail_queue";
    public static final String MAIL_EXCHANGE = "mail_exchange";
    public static final String MAIL_ROUTING_KEY = "mail_routingKey";

    public static final String MAIL_PAYMENT_QUEUE = "mail_payment_queue";
    public static final String MAIL_PAYMENT_EXCHANGE = "mail_payment_exchange";
    public static final String MAIL_PAYMENT_ROUTING_KEY = "mail_payment_routingKey";

    @Bean
    public Queue queueMail() {
        return new Queue(MAIL_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangeMail() {
        return new TopicExchange(MAIL_EXCHANGE);
    }

    @Bean
    public Binding bindingMail(@Qualifier("queueMail") Queue queue, @Qualifier("topicExchangeMail") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MAIL_ROUTING_KEY);
    }

    @Bean
    public Queue queuePaymentMail() {
        return new Queue(MAIL_PAYMENT_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangePaymentMail() {
        return new TopicExchange(MAIL_PAYMENT_EXCHANGE);
    }

    @Bean
    public Binding bindingPaymentMail(@Qualifier("queuePaymentMail") Queue queue, @Qualifier("topicExchangePaymentMail") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MAIL_PAYMENT_ROUTING_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}

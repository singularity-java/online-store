package kz.jusan.mailing.consumer;

import kz.jusan.mailing.config.MessagingConfig;
import kz.jusan.mailing.dto.OrderPostDto;
import kz.jusan.mailing.dto.PaymentConfirmDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import kz.jusan.mailing.service.MailService;

@Component
@Slf4j
@RequiredArgsConstructor
public class MailConsumer {
    private final MailService mailService;

    @RabbitListener(queues = MessagingConfig.MAIL_QUEUE)
    public void consumeMailMessage(OrderPostDto orderPostDto) {
        log.info("mail: " + orderPostDto);
        mailService.sendMail(orderPostDto.getOrderDto());
    }

    @RabbitListener(queues = MessagingConfig.MAIL_PAYMENT_QUEUE)
    public void consumePaymentMailMessage(PaymentConfirmDto paymentConfirmDto) {
        log.info("mail:" + paymentConfirmDto);
        mailService.sendMailPayment(paymentConfirmDto);
    }
}

package kz.jusan.mailing.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PaymentDto {
    private Long orderId;
    private String cardNumber;
}

# Online Store

![image](https://user-images.githubusercontent.com/54265853/180373106-be434c11-9364-4149-a2cf-a7cdd91ad7f6.png)

### В этом проекте вам необходимо реализовать систему для оформления и оплаты заказов в одном интернет магазине.

    C1, C2 - это клиенты, которые взаимодействуют с сервисами по http;
    RabbitMQ - брокер сообщений, который занимается созданием и управлением queue и exchange.
    Core - это сервис, который имеет доступ к базе данных. Реализует CRUD для заказов.
    Mailing - сервис для отправки писем.
    Order - сервис, который принимает запросы по ресурсу /order. Отвечает за создание заказа на определенную сумму. При получении запроса по ресурсу /order отправляет сообщение в Core для создания заказа. После успешного создания заказа отправляет письмо с информацией.
    Payment - сервис, который принимает запросы по ресурсу /payment. Отвечает за оплату заказа. При получении запроса по ресурсу /payment, сервис отправляет сообщение в Core за информацией о наличии заказа для оплаты. При отсутствии заказа, выдает ошибку. После успешной оплаты отправляет письмо с информацией о заказе.

## OrderAndPay

В данной системе пользователь имеет доступ только к двум ресурсам.

    POST /order - создает заказ на определенную сумму. Принимает в body - amount, email.
    POST /payment - оплачивает по карте сумму за определенный заказ. Принимает в body - card_number, в параметрах - id заказа.

## Spring Email

В сервисе **Mailing** в конфигурации *application.properties* для подключении к почте нужно добавить пароль от почты для приложения JavaMailSender.

```yml
spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=<your-email-here>
spring.mail.password=<your-password-here>
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
```

## Screenshots

### payment-notification-mail

![scr1](https://user-images.githubusercontent.com/54265853/180611369-a1250456-78d7-4786-9c46-73cadcaac90c.png)

### payment-confirmation-mail

![sc2](https://user-images.githubusercontent.com/54265853/180611386-097259f3-0eb8-4935-9736-c954495ed513.png)

### order-rest-api

![scrt2](https://user-images.githubusercontent.com/54265853/180925227-ef217a7f-fa47-44e1-83b6-79435f3fd88a.png)

### payment-rest-api

![scrpt3](https://user-images.githubusercontent.com/54265853/180925261-83a66f7f-c2e6-4f9f-b499-e63efdca61e8.png)

## Testing

There are several unit test cases to check if order and payment is created correctly.

![test1](https://user-images.githubusercontent.com/54265853/180925339-a7ebc234-d979-49be-a7cd-68029e18f824.png)


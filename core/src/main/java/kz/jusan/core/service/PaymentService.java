package kz.jusan.core.service;

import kz.jusan.core.dto.PaymentDto;

public interface PaymentService {
    void addPayment(PaymentDto paymentDto);
}

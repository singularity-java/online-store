package kz.jusan.core.service;

import kz.jusan.core.dto.OrderDto;
import kz.jusan.core.entity.CustomOrder;
import kz.jusan.core.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public void addOrder(OrderDto orderDto) {
        CustomOrder order = CustomOrder.builder()
                .amount(orderDto.getAmount())
                .email(orderDto.getEmail())
                .build();

        orderRepository.save(order);
    }

    @Override
    public CustomOrder getOrderById(Long id) {
        return orderRepository.findCustomOrderById(id);
    }
}

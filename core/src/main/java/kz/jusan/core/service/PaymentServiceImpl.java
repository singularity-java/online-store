package kz.jusan.core.service;

import kz.jusan.core.dto.PaymentDto;
import kz.jusan.core.entity.Payment;
import kz.jusan.core.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    @Override
    public void addPayment(PaymentDto paymentDto) {
        log.info("Payment:" + paymentDto);
        Payment payment = Payment.builder()
                .orderId(paymentDto.getOrderId())
                .cardNumber(paymentDto.getCardNumber())
                .build();
        paymentRepository.save(payment);
    }
}

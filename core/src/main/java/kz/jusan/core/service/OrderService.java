package kz.jusan.core.service;

import kz.jusan.core.dto.OrderDto;
import kz.jusan.core.entity.CustomOrder;

public interface OrderService {

    void addOrder(OrderDto orderDto);

    CustomOrder getOrderById(Long id);
}

package kz.jusan.core.repository;

import kz.jusan.core.entity.CustomOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<CustomOrder, Long> {
    CustomOrder findCustomOrderById(Long id);
}

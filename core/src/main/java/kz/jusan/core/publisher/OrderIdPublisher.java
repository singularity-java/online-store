package kz.jusan.core.publisher;

import kz.jusan.core.config.MessagingConfig;
import kz.jusan.core.dto.ValidOrderId;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrderIdPublisher {
    private final RabbitTemplate rabbitTemplate;

    public void publishOrderId(ValidOrderId validOrderId) {
        log.info("" + validOrderId);
        rabbitTemplate.convertAndSend(MessagingConfig.ORDER_ID_EXCHANGE, MessagingConfig.ORDER_ID_ROUTING_KEY, validOrderId);
    }
}

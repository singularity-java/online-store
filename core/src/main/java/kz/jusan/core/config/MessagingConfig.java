package kz.jusan.core.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

    public static final String ORDER_QUEUE = "order_queue";
    public static final String ORDER_EXCHANGE = "order_exchange";
    public static final String ORDER_ROUTING_KEY = "order_routingKey";

    public static final String PAYMENT_QUEUE = "payment_queue";
    public static final String PAYMENT_EXCHANGE = "payment_exchange";
    public static final String PAYMENT_ROUTING_KEY = "payment_routingKey";

    public static final String MAIL_PAYMENT_QUEUE = "mail_payment_queue";
    public static final String MAIL_PAYMENT_EXCHANGE = "mail_payment_exchange";
    public static final String MAIL_PAYMENT_ROUTING_KEY = "mail_payment_routingKey";

    public static final String ORDER_ID_QUEUE = "order_id_queue";
    public static final String ORDER_ID_EXCHANGE = "order_id_exchange";
    public static final String ORDER_ID_ROUTING_KEY = "order_id_routingKey";

    @Bean
    public Queue queueOrder() {
        return new Queue(ORDER_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangeOrder() {
        return new TopicExchange(ORDER_EXCHANGE);
    }

    @Bean
    public Binding bindingOrder(@Qualifier("queueOrder") Queue queue, @Qualifier("topicExchangeOrder") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ORDER_ROUTING_KEY);
    }

    @Bean
    public Queue queuePayment() {
        return new Queue(PAYMENT_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangePayment() {
        return new TopicExchange(PAYMENT_EXCHANGE);
    }

    @Bean
    public Binding bindingPayment(@Qualifier("queuePayment") Queue queue, @Qualifier("topicExchangePayment") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(PAYMENT_ROUTING_KEY);
    }

    @Bean
    public Queue queuePaymentMail() {
        return new Queue(MAIL_PAYMENT_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangePaymentMail() {
        return new TopicExchange(MAIL_PAYMENT_EXCHANGE);
    }

    @Bean
    public Binding bindingPaymentMail(@Qualifier("queuePaymentMail") Queue queue, @Qualifier("topicExchangePaymentMail") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MAIL_PAYMENT_ROUTING_KEY);
    }

    @Bean
    public Queue queueOrderId() {
        return new Queue(ORDER_ID_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangeOrderId() {
        return new TopicExchange(ORDER_ID_EXCHANGE);
    }

    @Bean
    public Binding bindingPaymentOrderId(@Qualifier("queueOrderId") Queue queue, @Qualifier("topicExchangeOrderId") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ORDER_ID_ROUTING_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}


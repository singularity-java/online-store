package kz.jusan.core.consumer;

import kz.jusan.core.config.MessagingConfig;
import kz.jusan.core.dto.ValidOrderId;
import kz.jusan.core.dto.PaymentConfirmDto;
import kz.jusan.core.dto.PaymentPostDto;
import kz.jusan.core.entity.CustomOrder;
import kz.jusan.core.publisher.OrderIdPublisher;
import kz.jusan.core.service.OrderService;
import kz.jusan.core.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class PaymentConsumer {
    private final PaymentService paymentService;
    private final OrderService orderService;
    private final OrderIdPublisher orderIdPublisher;
    private final RabbitTemplate rabbitTemplate;
    @RabbitListener(queues = MessagingConfig.PAYMENT_QUEUE)
    public void consumePayment(PaymentPostDto paymentPostDto) {
        log.info("RabbitListener: " + paymentPostDto);
        paymentService.addPayment(paymentPostDto.getPaymentDto());

        Long orderId = paymentPostDto.getPaymentDto().getOrderId();
        CustomOrder order = orderService.getOrderById(orderId);
        if(order != null) {
            String email = order.getEmail();
            orderIdPublisher.publishOrderId(new ValidOrderId(true, orderId));
            PaymentConfirmDto paymentConfirmDto = new PaymentConfirmDto(
                    email,
                    "Payment Confirmation",
                    "Congratulations! You have successfully purchased our brand-new product. Have a good day!");
            rabbitTemplate.convertAndSend(MessagingConfig.MAIL_PAYMENT_EXCHANGE,
                    MessagingConfig.MAIL_PAYMENT_ROUTING_KEY,
                    paymentConfirmDto);

        } else {
            log.info("Order is null, order id is not correct");
            orderIdPublisher.publishOrderId(new ValidOrderId(false, orderId));
        }

    }
}

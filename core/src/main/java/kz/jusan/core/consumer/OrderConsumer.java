package kz.jusan.core.consumer;

import kz.jusan.core.config.MessagingConfig;
import kz.jusan.core.dto.OrderPostDto;
import kz.jusan.core.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Slf4j
public class OrderConsumer {
    private final OrderService orderService;

    @RabbitListener(queues = MessagingConfig.ORDER_QUEUE)
    public void consumeMessageFromQueue(OrderPostDto orderPostDto) {
        log.info("OrderPostDto: " + orderPostDto);
        orderService.addOrder(orderPostDto.getOrderDto());
    }
}

package kz.jusan.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaymentPostDto {
    private PaymentDto paymentDto;
    private String status;
    private String message;
}

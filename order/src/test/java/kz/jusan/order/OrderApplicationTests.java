package kz.jusan.order;

import kz.jusan.order.dto.OrderDto;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class OrderApplicationTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;

	@Before
	void setUp() {

	}

	@Test
	void contextLoads() {
	}

	@Test
	void orderCreate() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		OrderDto orderDto = new OrderDto(200.0, "mtorezhan@gmail.com");
		String json = "{ 'amount': 200, 'email': 'makhambet.torezhanalumni.nu.edu.kz' }";
		mvc.perform(MockMvcRequestBuilders.post("/api/v1/app/order")
						.content(json))
				.andExpect(status().is4xxClientError());
	}
}

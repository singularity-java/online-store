package kz.jusan.order.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class EmailNotValidAdvice {
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> accountNotFound(MethodArgumentNotValidException e) {
        EmailNotValidException emailNotValidException = new EmailNotValidException("Email is not valid. Please try again", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(emailNotValidException, HttpStatus.BAD_REQUEST);
    }
}


package kz.jusan.order.advice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailNotValidException {
    private String message;
    private HttpStatus httpStatus;
}

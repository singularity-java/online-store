package kz.jusan.order.controller;

import jakarta.validation.Valid;
import kz.jusan.order.config.MessagingConfig;
import kz.jusan.order.dto.OrderDto;
import kz.jusan.order.dto.OrderPostDto;
import kz.jusan.order.dto.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/app/order")
@Slf4j
@RequiredArgsConstructor
public class OrderController {
    private final RabbitTemplate rabbitTemplate;
    @PostMapping
    public ResponseEntity<Object> createOrder(@Valid @RequestBody OrderDto orderDto) {
        log.info("Order dto created: " + orderDto);
        OrderPostDto orderPostDto = new OrderPostDto(orderDto, "PROCESS", "Order placed successfully into queue");
        rabbitTemplate.convertAndSend(MessagingConfig.ORDER_EXCHANGE, MessagingConfig.ORDER_ROUTING_KEY, orderPostDto);
        rabbitTemplate.convertAndSend(MessagingConfig.MAIL_EXCHANGE, MessagingConfig.MAIL_ROUTING_KEY, orderPostDto);
        ResponseDto responseDto = new ResponseDto("Order created", orderDto, HttpStatus.OK);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}

package kz.jusan.payment;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class PaymentApplicationTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	@Test
	void contextLoads() {
	}

	@Test
	void paymentCreate() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		String json = "{ 'accountId': 1, 'cardNumber': '77777777777777' }";
		mvc.perform(MockMvcRequestBuilders.post("/api/v1/app/payment")
						.content(json))
				.andExpect(status().is4xxClientError());
	}
}

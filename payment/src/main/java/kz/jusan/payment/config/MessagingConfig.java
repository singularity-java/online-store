package kz.jusan.payment.config;


import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

    public static final String PAYMENT_QUEUE = "payment_queue";
    public static final String PAYMENT_EXCHANGE = "payment_exchange";
    public static final String PAYMENT_ROUTING_KEY = "payment_routingKey";
    public static final String MAIL_QUEUE = "mail_queue";
    public static final String MAIL_EXCHANGE = "mail_exchange";
    public static final String MAIL_ROUTING_KEY = "mail_routingKey";

    public static final String ORDER_ID_QUEUE = "order_id_queue";
    public static final String ORDER_ID_EXCHANGE = "order_id_exchange";
    public static final String ORDER_ID_ROUTING_KEY = "order_id_routingKey";
    @Bean
    public Queue queuePayment() {
        return new Queue(PAYMENT_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangePayment() {
        return new TopicExchange(PAYMENT_EXCHANGE);
    }

    @Bean
    public Binding binding(@Qualifier("queuePayment") Queue queue, @Qualifier("topicExchangePayment") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(PAYMENT_ROUTING_KEY);
    }

    @Bean
    public Queue queueOrderId() {
        return new Queue(ORDER_ID_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangeOrderId() {
        return new TopicExchange(ORDER_ID_EXCHANGE);
    }

    @Bean
    public Binding bindingPaymentOrderId(@Qualifier("queueOrderId") Queue queue, @Qualifier("topicExchangeOrderId") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ORDER_ID_ROUTING_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

    @Bean
    public Queue queueMail() {
        return new Queue(MAIL_QUEUE);
    }

    @Bean
    public TopicExchange topicExchangeMail() {
        return new TopicExchange(MAIL_EXCHANGE);
    }

    @Bean
    public Binding bindingMail(@Qualifier("queueMail") Queue queue, @Qualifier("topicExchangeMail") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MAIL_ROUTING_KEY);
    }
}

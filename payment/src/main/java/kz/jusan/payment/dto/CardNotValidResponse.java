package kz.jusan.payment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardNotValidResponse {
    private String message;
    private HttpStatus httpStatus;
}

package kz.jusan.payment.advice;

import kz.jusan.payment.dto.OrderIdNotValidResponse;
import kz.jusan.payment.exception.OrderIdNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class OrderIdNotValidAdvice {
    @ResponseBody
    @ExceptionHandler(OrderIdNotValidException.class)
    public ResponseEntity<Object> handleNotValidOrderId(OrderIdNotValidException e) {
        OrderIdNotValidResponse response = new OrderIdNotValidResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}

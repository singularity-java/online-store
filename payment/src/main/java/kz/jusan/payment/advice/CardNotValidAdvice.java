package kz.jusan.payment.advice;

import kz.jusan.payment.dto.CardNotValidResponse;
import kz.jusan.payment.exception.CardNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CardNotValidAdvice {
    @ResponseBody
    @ExceptionHandler(CardNotValidException.class)
    public ResponseEntity<Object> handleCardNotValidException(CardNotValidException e) {
        CardNotValidResponse response = new CardNotValidResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}

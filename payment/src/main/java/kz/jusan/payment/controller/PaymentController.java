package kz.jusan.payment.controller;

import kz.jusan.payment.config.MessagingConfig;
import kz.jusan.payment.dto.*;
import kz.jusan.payment.exception.CardNotValidException;
import kz.jusan.payment.exception.OrderIdNotValidException;
import kz.jusan.payment.service.CreditCardValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/app/payment")
@Slf4j
@RequiredArgsConstructor
public class PaymentController {
    private final RabbitTemplate rabbitTemplate;

    private ValidOrder validOrder;
    @PostMapping
    public ResponseEntity<Object> createPayment(@RequestBody PaymentDto paymentDto) throws CardNotValidException, OrderIdNotValidException {
        log.info("Payment created: " + paymentDto);

        Long cardNumber = Long.parseLong(paymentDto.getCardNumber());
        if(!CreditCardValidator.isValid(cardNumber))
            throw new CardNotValidException("Credit card is not valid either by VISA or MasterCard standards");


        PaymentPostDto paymentPostDto = new PaymentPostDto(paymentDto, "PROCESS", "Payment placed successfully");
        rabbitTemplate.convertAndSend(MessagingConfig.PAYMENT_EXCHANGE, MessagingConfig.PAYMENT_ROUTING_KEY, paymentPostDto);

        if(validOrder != null) {
            if(!validOrder.isValid()) {
                throw new OrderIdNotValidException("Order id is not valid");
            }
        }

        ResponseDto responseDto = new ResponseDto("Payment created", paymentDto, HttpStatus.OK);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @RabbitListener(queues = MessagingConfig.ORDER_ID_QUEUE)
    public void consumeOrderId(ValidOrderId validOrderId) {

        if(validOrderId != null) {
            if(validOrderId.isValid())
                validOrder = new ValidOrder(true);
            else
                validOrder = new ValidOrder(false);
        } else {
            validOrder = new ValidOrder(false);
        }
    }
}
